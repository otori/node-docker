#!/bin/sh
# Starts the Tezos node client
# Written by Luke Youngblood, luke@blockscale.net

init_node() {
	tezos-node identity generate 26
	tezos-node config init \
		--rpc-addr="[::]:8732" \
		--net-addr="[::]:9732" \
		--network='mainnet' \
		--history-mode full \
		--cors-origin='*' \
		--cors-header 'Origin, X-Requested-With, Content-Type, Accept, Range'
	cat /home/tezos/.tezos-node/config.json
}

start_node() {
	tezos-node run --rpc-addr 0.0.0.0:8732 --allow-all-rpc 0.0.0.0:8732
    if [ $? -ne 0 ]
	then
    	echo "Node failed to start; exiting."
    	exit 1
	fi
}

# main

init_node
# TODO HERE: restore a snapshot
start_node
